import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Team Football',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Team Football'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  ///int _counter = 0;

  // void _incrementCounter() {
  //   setState(() {
  //     // This call to setState tells the Flutter framework that something has
  //     // changed in this State, which causes it to rerun the build method below
  //     // so that the display can reflect the updated values. If we changed
  //     // _counter without calling setState(), then the build method would not be
  //     // called again, and so nothing would appear to happen.
  //    // _counter++;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
        
      ),
      body: ListView(
        padding: new EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
        children: <Widget>[
          Card(
            shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(16.0)
            ),
            child: Column(
              children: <Widget>[
                new ClipRRect(
                  child: Image.network(
                    'https://cdn0-production-images-kly.akamaized.net/W2P2mWKoHSXbQlZKLfukc8yTrH8=/640x360/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/1399592/original/052424400_1478642727-475853338.jpg'
                  ),
                  borderRadius: BorderRadius.only(
                    topLeft: new Radius.circular(16.0),
                    topRight: new Radius.circular(16.0)
                  ),
                ),
                new Padding(
                  padding: new EdgeInsets.all(16.0),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                          new Text('Barcelona',
                            style: Theme.of(context).textTheme.title),
                            new Row(
                              children: <Widget>[
                                new Text(''),
                            ],
                            ),
                            new Row(
                              children: <Widget>[
                                Flexible(
                                  child: Text('Fútbol Club Barcelona, juga dikenal sebagai Barcelona atau Barça, adalah klub sepak bola profesional yang berbasis di Barcelona, Catalunya, Spanyol.'),
                                ), 
                            ],
                            ),
                        ],
                  ),
                )
              ],
            ),
          ),
          Card(
            shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(16.0)
            ),
            child: Column(
              children: <Widget>[
                new ClipRRect(
                  child: Image.network(
                    'https://ak4.picdn.net/shutterstock/videos/1015023244/thumb/1.jpg'
                  ),
                  borderRadius: BorderRadius.only(
                    topLeft: new Radius.circular(16.0),
                    topRight: new Radius.circular(16.0)
                  ),
                ),
                new Padding(
                  padding: new EdgeInsets.all(16.0),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                          new Text('Real Madrid',
                            style: Theme.of(context).textTheme.title),
                            new Row(
                              children: <Widget>[
                                new Text(''),
                            ],
                            ),
                            new Row(
                              children: <Widget>[
                                Flexible(
                                  child: Text('Real Madrid Club de Fútbol (pengucapan bahasa Spanyol: [reˈal maˈðɾið ˈkluβ ðe ˈfutβol]; Royal Madrid Football Club), umumnya dikenal sebagai Real Madrid, adalah klub sepak bola profesional yang berbasis di Madrid, Spanyol.'),
                                ), 
                            ],
                            ),
                        ],
                  ),
                )
              ],
            ),
          ), 
          Card(
            shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(16.0)
            ),
            child: Column(
              children: <Widget>[
                new ClipRRect(
                  child: Image.network(
                    'https://asset.indosport.com/article/image/q/80/200791/624328970-169.jpg?w=750&h=423'
                  ),
                  borderRadius: BorderRadius.only(
                    topLeft: new Radius.circular(16.0),
                    topRight: new Radius.circular(16.0)
                  ),
                ),
                new Padding(
                  padding: new EdgeInsets.all(16.0),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                          new Text('Manchester United',
                            style: Theme.of(context).textTheme.title),
                            new Row(
                              children: <Widget>[
                                new Text(''),
                            ],
                            ),
                            new Row(
                              children: <Widget>[
                                Flexible(
                                  child: Text('Manchester United Football Club adalah sebuah klub sepak bola profesional Inggris yang berbasis di Old Trafford, Manchester Raya, yang bermain di Liga Inggris. Didirikan sebagai Newton Heath LYR Football Club pada tahun 1878, klub ini berganti nama menjadi Manchester United pada 1902 dan pindah ke Old Trafford pada tahun 1910.'),
                                ), 
                            ],
                            ),
                        ],
                  ),
                )
              ],
            ),
          ), 
          Card(
            shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(16.0)
            ),
            child: Column(
              children: <Widget>[
                new ClipRRect(
                  child: Image.network(
                    'https://cdn.akurat.co/images/uploads/images/akurat_20190127033457_T4177T.jpg'
                  ),
                  borderRadius: BorderRadius.only(
                    topLeft: new Radius.circular(16.0),
                    topRight: new Radius.circular(16.0)
                  ),
                ),
                new Padding(
                  padding: new EdgeInsets.all(16.0),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                          new Text('Manchester City',
                            style: Theme.of(context).textTheme.title),
                            new Row(
                              children: <Widget>[
                                new Text(''),
                            ],
                            ),
                            new Row(
                              children: <Widget>[
                                Flexible(
                                  child: Text('Manchester City Football Club (dikenal pula sebagai Man City atau The Citizens) adalah sebuah klub sepak bola profesional dari Inggris yang bermain di Liga Premier Inggris. Klub ini merupakaan klub sekota dengan Manchester United dan bermarkas di Stadion Etihad, Manchester.'),
                                ), 
                            ],
                            ),
                        ],
                  ),
                )
              ],
            ),
          ), 
          Card(
            shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(16.0)
            ),
            child: Column(
              children: <Widget>[
                new ClipRRect(
                  child: Image.network(
                    'https://m.ayobogor.com/images-bogor/post/articles/2017/08/28/675/liverpool_reuters_phil_noble.jpg'
                  ),
                  borderRadius: BorderRadius.only(
                    topLeft: new Radius.circular(16.0),
                    topRight: new Radius.circular(16.0)
                  ),
                ),
                new Padding(
                  padding: new EdgeInsets.all(16.0),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                          new Text('Liverpool',
                            style: Theme.of(context).textTheme.title),
                            new Row(
                              children: <Widget>[
                                new Text(''),
                            ],
                            ),
                            new Row(
                              children: <Widget>[
                                Flexible(
                                  child: Text('Liverpool Football Club /ˈlɪvərpuːl/ (dikenal pula sebagai Liverpool atau The Reds) adalah sebuah klub sepak bola asal Inggris yang berbasis di Kota Liverpool. Saat ini Liverpool adalah peserta Liga Utama Inggris.'),
                                ), 
                            ],
                            ),
                        ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
